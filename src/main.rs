#[macro_use]
extern crate clap;
extern crate flate2;

use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::ops::Rem;

use clap::App;
use flate2::read::MultiGzDecoder;

fn main() {
    let yaml = load_yaml!("../cli.yml");
    let matches = App::from_yaml(yaml).get_matches();
    let file_names = matches.values_of("fastq").unwrap();
    let files_count = file_names.len();

    print!("{}", file_names.map(|file_name| {
        let file = File::open(file_name).unwrap();
        let decoder = MultiGzDecoder::new(file);
        let lines = BufReader::new(decoder).lines().map(|r| r.unwrap());
        mean_read_quality(lines)
    }).sum::<f32>() / files_count as f32);
}


fn mean_read_quality<T: Iterator<Item=String>>(fastq_lines: T) -> f32 {
    let (reads_count, total_quality) = fastq_lines.enumerate()
        .filter(|(i, _)| (i + 1).rem(4) == 0)
        .fold((0, 0.0), |acc, (_, line)| {
            (
                acc.0 + 1,
                acc.1 + line.chars().map(|c| (c as usize) - 33).sum::<usize>() as f32 / line.len() as f32
            )
        });
    total_quality / reads_count as f32
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_mean_read_quality() {
        let a = vec![
            "@M04489:46:000000000-D48B7:1:1101:15203:1343 1:N:0:26".to_string(),
            "AGGTGTGAAGAAATCCTGCATCTCAGTCCCACACAGGCAGCTGTCTCAGGCTACAGAACACAATAGTCATGAACAAATTCAGGTCAGTCATGGTAAGTGATGACACTCTGAACAGCTCACCACACATTCGAAACGTCCCTATCAAAGGATC".to_string(),
            "+".to_string(),
            "AABCBFCFFFFFGGGGGGGGGGHHHHHHHHHHGGGHHGHGGHHHHHHHHHHHHHHHGHHHHHGHHHHHHHHHHHHHHHHHGFBDGGH5DBGGHGHFHHGFGHHHH@BBGGHGHHHHHHHHHHHHGGHHHGHFGEFFGFFHHHHBFGFHHBF".to_string(),
        ].into_iter();
        assert_eq!(37.81457, mean_read_quality(a));
    }
}